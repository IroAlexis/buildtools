# Introduction

Scripts that make it easy to build Wine, ~~DXVK and VKD3D-Proton~~ from source code.

Inspired by [wine-tkg-git](https://github.com/Frogging-Family/wine-tkg-git)


# Wine
## Enable NTsync
* Load ntsync module at boot with file `/etc/modules-load.d/ntsync.conf`
```
ntsync
```
* Reboot and verify if device `ntsync` is loading in `/dev`

## Enable damavand
```
export WINE_D3D_CONFIG="renderer=vulkan;VideoPciVendorID=0xc0de"
```

## Use FFmpeg backend
```reg
[HKEY_CURRENT_USER\\Software\\Wine\\MediaFoundation]
DisableGstByteStreamHandler=dword:00000001
```
https://gitlab.winehq.org/wine/wine/-/merge_requests/6442


# Acknowledgements
* [TkG](https://github.com/Tk-Glitch)
