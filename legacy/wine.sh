#!/usr/bin/env bash

# buildtools - Script for build lastest wine-git
# Copyright (C) 2023  IroAlexis <iroalexis@outlook.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Based on TkG's work https://github.com/Frogging-Family/wine-tkg-git

set -e


function _error
{
	echo -e "\033[1;31m==> ERROR: $1\033[1;0m" >&2
}

function _msg
{
	echo -e "\033[1;34m->\033[1;0m \033[1;1m$1\033[1;0m" >&2
}

function _warning
{
	echo -e "\033[1;33m==> WARNING: $1\033[1;0m" >&2
}

function _run_patcher
{
	if ! git -C "${_src_path}" apply "$1"
	then
		_error "$(basename "$1")"
		exit 1
	fi
}


function apply_patches
{
	for _patch in "${_basedir}/patches/$1/"*.patch
	do
		_run_patcher "${_patch}"
	done
}

function apply_userpatches
{
	for _patch in "${_basedir}"/userpatches/*.patch
	do
		if [ -e "${_patch}" ]
		then
			local _name
			local _rslt
			_name="$(basename -s .patch "${_patch}")"

			if [[ ${_name} =~ ^[0-9]+$ ]]
			then
				_msg "https://gitlab.winehq.org/wine/wine/-/merge_requests/${_name}"
			else
				_msg "${_name}.patch"
			fi

			read -rp "Do you want apply this patch? [N/y] " _rslt
			if [[ ${_rslt} =~ [Yy] ]]
			then
				_run_patcher "${_patch}"
			fi
		fi
	done
}

function enable_ccache
{
	if command -v ccache &>/dev/null
	then
		CC="ccache gcc"
		export CC
		CXX="ccache g++"
		export CXX
		CROSSCC="ccache x86_64-w64-mingw32-gcc"
		export CROSSCC
		x86_64_CC="$CROSSCC"
		export x86_64_CC

		# Required for new-style WoW64 builds (otherwise 32-bit portions won't be ccached)
		i386_CC="ccache i686-w64-mingw32-gcc"
		export i386_CC
	else
		_warning "ccache not installed"
	fi
}

configure()
{
	enable_ccache

	"${_src_path}"/configure \
		--prefix="${_dest_path}" \
		--enable-win64 \
		--enable-archs=i386,x86_64 \
		--disable-tests
}

function prepare
{
	git add .

 	./dlls/winevulkan/make_vulkan
 	./tools/make_requests
	./tools/make_specfiles
	./tools/make_makefiles

	autoreconf -fi
}

function usage
{
	cat <<EOF
Usage: $0 WINE_SOURCE PREFIX
EOF
}


# Initialize variables and parse arguments
if [ ! $# -eq 2 ]
then
	usage
	exit 1
fi

_basedir="$(realpath "$(dirname "$0")")"
_src_orig="$(realpath "$1")"
_dest_path="$(realpath "$2")"
_pkgname="$(basename "${_src_orig}")"

if [ ! -d "${_src_orig}" ] || [ ! -d "${_src_orig}/.git" ]
then
	_error "${_src_orig}: Not git directory."
	exit 1
fi
if [ ! -d "${_dest_path}" ]
then
	_error "${_dest_path}: Not directory."
	exit 1
fi


# Copying source into /tmp/buildtools
_tmpdir="$(mktemp -d)"

_msg "Copying project in ${_tmpdir}... Please be patient."
cp --reflink=auto -urt "${_tmpdir}" "${_src_orig}"

_src_path="${_tmpdir}/${_pkgname}"
_build_path="${_src_path}.build"
mkdir -p "${_build_path}"


# Trying apply ntsync patch
if [ -b "/dev/ntsync" ]
then
	_pkgname="${_pkgname}-fastsync"
	_msg "Applying fastsync patches..."
	apply_patches fastsync
fi
apply_userpatches
_dest_path="${_dest_path}/${_pkgname}"


(cd "${_src_path}" && prepare)
(cd "${_build_path}" && configure)
make -C "${_build_path}" -j"$(nproc)"


_msg "Installing to ${_dest_path}..."
if make -C "${_build_path}" install
then
	# Backup compiled wine version
	git -C "${_src_path}" describe --tags --long > "${_dest_path}/share/wine/version"

	_msg "Wine build available here: ${_dest_path}"
fi


exit 0
